# jQuery SVG Meter Plugin 1.0.1
## Summary
- - -
**jQuery SVG Meter Plugin** is a JavaScript plugin for creating simply SVG meter widgets.

**jQuery SVG Meter Plugin requires [Raphael][1]**, a JavaScript Library for drawing cross-browser vector graphics. (Raphael 2.x or higher is recommended)

## Note
- - -
This plugin [duck-punches][2] jQuery's $.fn.val() method so that you can get/set the meter widget's value conveniently.

## Demo

A [demo][3] is available at CodePen

![Screen Shot](https://bitbucket.org/terryyounghk/jquery-svg-meter-plugin/raw/cc6331d8cc0bc268554072eda502505c4cbc762e/screenshot.png)

## Usage
- - -

The following guides you to how set this up quickly. API Documentation is on the [Wiki][4] page.

### References
Include both jQuery and Raphael on your page, then include jquery-svg-meter.js. For example:

    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script type="text/javascript" src="js/jquery-svg-meter.js"></script>

### Instantiation
Given the following HTML serving as a container for the SVG element

    <div id="meter" class="meter"></div>
    
    <script>
    $(document).ready(function () {
        $('#meter').svgMeter();
    });
    </script>

By default, the value of the meter widget is set to its minValue, which by default is zero. 

You can instantiate the meter widget with a known value as follows:

    $('#meter').svgMeter({
        value: 100
    });

Once instantiated, you can get/set the value of the meter using .val()

    $('#meter').val(55); // set to 55
    var value = $('#meter').val(); // get the value

Other than the meter's value, there is currently no 'refresh' facility to change the configuration of the meter widget once its instantiated. You may need to destroy the instance and re-instantiate with new options manually.

### Styling

The jQuery SVG Meter Plugin only draws the meter ticks, the meter values and the meter hand.

It does not draw any background colors or background images.

For that, you can simply style the container DIV.

## API Documentation

Please visit the [Wiki][4] page.

## License

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)

WTFPL

## Author

Terry Young

    
[1]: http://www.raphaeljs.com/
[2]: http://paulirish.com/2010/duck-punching-with-jquery/
[3]: http://codepen.io/terryyounghk/pen/FDyrg
[4]: https://bitbucket.org/terryyounghk/jquery-svg-meter-plugin/wiki